FROM debian:stretch

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 LC_COLLATE=C DEBIAN_FRONTEND=noninteractive

RUN apt-get update --fix-missing && apt-get install -y wget bzip2 ca-certificates \
    libglib2.0-0 libxext6 libsm6 libxrender1 \
    git mercurial subversion

RUN echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh && \
    wget --quiet https://repo.continuum.io/archive/Anaconda3-5.1.0-Linux-x86_64.sh -O ~/anaconda.sh && \
    /bin/bash ~/anaconda.sh -b -p /opt/conda && \
    rm ~/anaconda.sh

RUN apt-get install -y curl grep sed dpkg && \
    TINI_VERSION=`curl https://github.com/krallin/tini/releases/latest | grep -o "/v.*\"" | sed 's:^..\(.*\).$:\1:'` && \
    curl -L "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini_${TINI_VERSION}.deb" > tini.deb && \
    dpkg -i tini.deb && \
    rm tini.deb && \
    apt-get clean

ENV PATH /opt/conda/bin:$PATH

RUN set -ex \
    && apt-get update -y \
    && apt-get install -q -y --no-install-recommends \
        git gcc gfortran make gettext yasm gdal-bin  \
        build-essential cmake pkg-config default-jdk ant doxygen unzip wget \
        libc6-dev zlib1g-dev musl-dev libpq-dev libexpat1-dev \
        libxml2-dev libxslt1-dev libjansson-dev libpcre3-dev \
        libpng-dev libfreetype6-dev libjpeg-dev libffi-dev \
        libhdf5-dev libopenblas-dev liblapack-dev libcurl4-openssl-dev \
        libssl-dev libwebp-dev libopenexr-dev libav-tools \
        libgdal-dev libdc1394-22-dev libavcodec-dev libavformat-dev \
        python3-numpy python3-scipy python3-h5py python3-skimage \
        python3-matplotlib python3-pandas python3-sklearn python3-sympy \
    && rm -rf /var/cache/apt/* \
    && rm -rf /usr/share/man/* \
    && rm -rf /usr/share/doc/* \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --set libblas.so.3 /usr/lib/openblas-base/libblas.so.3 \
    && pip install --no-cache-dir --upgrade pip setuptools \
    && pip install --no-cache-dir --upgrade wheel

ENV TERM="xterm-256color" \
    PYTHONBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_TIMEOUT=60 \
    PIP_DISABLE_PIP_VERSION_CHECK=true \
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64

COPY docker-entrypoint.sh /sbin/
COPY requirements.txt /system-requirements.txt

RUN set -ex && conda update --yes -n base conda

RUN set -ex && cat /system-requirements.txt | xargs -L1 conda install --yes --quiet 

RUN set -ex \
    && conda install --yes -c conda-forge tensorflow \
    && pip install --no-cache-dir git+git://github.com/fchollet/keras.git@1.2.0 \
    && pip install --no-cache-dir git+git://github.com/Lasagne/Lasagne.git@v0.1

COPY jupyter_notebook_config.py /root/.jupyter/

RUN set -ex \
    && echo jupyter notebook \"\$\@\" > /usr/local/bin/jupyter.sh \
    && chmod +x /usr/local/bin/jupyter.sh

ENTRYPOINT ["tini", "--", "/sbin/docker-entrypoint.sh"]

EXPOSE 6006 8888

WORKDIR /root

CMD ["/bin/bash"]
